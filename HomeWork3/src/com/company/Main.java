package com.company;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


public class Main {

    public static void main(String[] args) {
        String textPesni = getTextFromFile("files/text.txt");
        System.out.println(textPesni);
        System.out.println("\n");

        List<String> words = getWordsFromText(textPesni);
        Map<String, Integer> wordCounts = getCountOfEveryWord(words);
        System.out.println(wordCounts);
        System.out.println("\n");

        Map<Character,Map<String, Integer>> wordsGroupedByLetter = groupWordsByLetter(wordCounts);
        System.out.println(wordsGroupedByLetter);
        System.out.println("\n");

        showMapWithLetters(wordsGroupedByLetter);
    }

    public static String getTextFromFile(String pathFile)
    {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(pathFile));
            return new String(encoded,StandardCharsets.UTF_8 );
        } catch (IOException e) {
            return "";
        }
    }

    public static List getWordsFromText(String text) {
        text = text.replaceAll("\\W+", " ");
        List<String> words = new ArrayList<>();

        for (String word : text.split(" ")) {
            words.add(word.toLowerCase());
        }
        return words;
    }

    public static Map getCountOfEveryWord(List<String> words) {
        Map<String, Integer> dictionary = new LinkedHashMap<>();
        Integer count;
        words.sort(Comparator.naturalOrder());

        for (String word : words) {

            count = dictionary.get(word);
            dictionary.put(word, count == null ? 1 : count + 1);
        }

        return dictionary;
    }

    public static  Map<Character,Map<String, Integer>> groupWordsByLetter(Map<String, Integer> words){
        Map<Character,Map<String, Integer>> result = new LinkedHashMap<>();
        Map<String, Integer> wordsInLetter;

        for (String word : words.keySet()) {
            Character letter = word.charAt(0);
            wordsInLetter = result.get(letter) == null ? new LinkedHashMap<>() : result.get(letter);
            wordsInLetter.put(word,words.get(word));
            result.put(letter, wordsInLetter);
        }

        return result;
    }

    public static void showMapWithLetters(Map<Character,Map<String, Integer>> map){
        map.forEach((key,value)->{
            System.out.print(Character.toUpperCase(key) + ":");
            showMapWithWords(value);
        });
    }

    public static void showMapWithWords(Map<String, Integer> map){
         map.forEach((key,value)->{
            System.out.println("\t" + key + " " + value);
        });

    }
}
