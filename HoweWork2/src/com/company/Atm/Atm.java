package com.company.Atm;

import com.company.Cards.Card;

import java.math.BigDecimal;

public class Atm {
    public Card card;

    public Atm(Card card) {
        setCard(card);
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public void withdrawal(BigDecimal sum) {
        if (card.isCanWithdrawal(sum)) {
            card.withdrawal(sum);
            System.out.println("Сумма на " + sum + " была снята со счета");
        } else {
            System.out.println("На счете не достаточно средсв");
        }
    }

    public void replenishment(BigDecimal sum) {
        card.replenishment(sum);
    }
}
