package com.company.Cards;

import java.math.BigDecimal;

public abstract class Card {
    private final String number;
    private final String holder;
    protected BigDecimal account;


    public Card(String number, String holder, BigDecimal account) {
        this.number = number;
        this.holder = holder;
        this.account = account;
    }

    public Card(String number, String holder) {
        this.number = number;
        this.holder = holder;
        this.account = BigDecimal.valueOf(0);
    }

    public void withdrawal(BigDecimal sum) {
        account = account.subtract(sum);
    }

    public void replenishment(BigDecimal sum) {
        account = account.add(sum);
    }

    abstract public boolean isCanWithdrawal(BigDecimal sum);

    public BigDecimal getAccount() {
        return account;
    }

    public String getNumber() {
        return number;
    }

    public String getHolder() {
        return holder;
    }
}
