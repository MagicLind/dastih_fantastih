package com.company.Cards;

import java.math.BigDecimal;

public class CreditCard extends Card {

    public CreditCard(String number, String holder, BigDecimal account) {
        super(number, holder, account);
    }

    public CreditCard(String number, String holder) {
        super(number, holder);
    }

    @Override
    public boolean isCanWithdrawal(BigDecimal sum) {
        return true;
    }
}
