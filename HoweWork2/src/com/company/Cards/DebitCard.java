package com.company.Cards;

import java.math.BigDecimal;

public class DebitCard extends Card {

    public DebitCard(String number, String holder, BigDecimal account) {
        super(number, holder, account);
    }

    public DebitCard(String number, String holder) {
        super(number, holder);
    }

    @Override
    public boolean isCanWithdrawal(BigDecimal sum) {
        if(account.compareTo(sum) == -1){
            return false;
        }

        return true;
    }
}
