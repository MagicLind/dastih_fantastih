package com.company.superHashSet;

import java.util.HashSet;
import java.util.Set;

public class SuperHashSet extends HashSet {

    public static Set union(Set firstSet, Set secondSet) {
        Set result = new SuperHashSet();
        result.addAll(firstSet);
        result.addAll(secondSet);

        return result;
    }

    public static Set intersection(Set firstSet, Set secondSet) {
        Set result = new SuperHashSet();
        result.addAll(firstSet);
        result.retainAll(secondSet);

        return result;
    }

    public static Set minus(Set firstSet, Set secondSet) {
        Set result = new SuperHashSet();
        result.addAll(firstSet);
        result.removeAll(secondSet);

        return result;
    }

    public static Set difference(Set firstSet, Set secondSet) {
        Set union = union(firstSet, secondSet);
        Set intersection = intersection(firstSet, secondSet);

        return minus(union, intersection);
    }
}
