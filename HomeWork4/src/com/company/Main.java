package com.company;

import com.company.superHashSet.SuperHashSet;

import java.util.Set;

public class Main {

    public static final Set<Integer> FIRST_SET = new SuperHashSet() {{
        add(1);
        add(2);
        add(3);
        add(4);
    }};

    public static final Set<Integer> SECOND_SET = new SuperHashSet() {{
        add(3);
        add(4);
        add(6);
        add(9);
        add(5);
    }};
    ;

    public static void main(String[] args) {
        System.out.println(SuperHashSet.union(FIRST_SET, SECOND_SET));
        System.out.println(SuperHashSet.intersection(FIRST_SET, SECOND_SET));
        System.out.println(SuperHashSet.minus(FIRST_SET, SECOND_SET));
        System.out.println(SuperHashSet.difference(FIRST_SET, SECOND_SET));
    }
}
